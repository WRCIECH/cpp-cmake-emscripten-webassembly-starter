if(EMSCRIPTEN_VERSION STREQUAL "")
    message(FATAL_ERROR "EMSCRIPTEN_VERSION cache variable has to be specified in the preset file!")
endif()

find_package(Git)
if(Git_FOUND)
    message("Git found: ${GIT_EXECUTABLE}")
endif()

if(DOWNLOAD_EMSCRIPTEN)
    message(STATUS "bin dir: ${CMAKE_BINARY_DIR}")
    # Clone repo if it doesn't exist
    if(NOT EXISTS ${CMAKE_BINARY_DIR}/emsdk/.git)
        execute_process(
                COMMAND ${GIT_EXECUTABLE} clone https://github.com/emscripten-core/emsdk.git
                WORKING_DIRECTORY ${CMAKE_BINARY_DIR}
                OUTPUT_QUIET
                RESULT_VARIABLE result
        )
        if(result)
            message(FATAL_ERROR "Could not clone EMSDK repository. Error message: ${result}!")
        endif()
    endif()

    # Install specified emsdk version
    execute_process(
            COMMAND ./emsdk install ${EMSCRIPTEN_VERSION}
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/emsdk/"
            OUTPUT_QUIET
            ECHO_ERROR_VARIABLE
            RESULT_VARIABLE result
    )
    if(result)
        message(FATAL_ERROR "Could not install ${EMSCRIPTEN_VERSION}. Error message: ${result}!")
    endif()

    # Activate specified emsdk version
    execute_process(
            COMMAND ./emsdk activate ${EMSCRIPTEN_VERSION}
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}/emsdk/"
            OUTPUT_QUIET
            ECHO_ERROR_VARIABLE
            RESULT_VARIABLE result
    )
    if(result)
        message(FATAL_ERROR "Could not activate ${EMSCRIPTEN_VERSION}. Error message: ${result}!")
    endif()
endif()
